module github.com/sample-apps

go 1.16

require (
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/jmoiron/sqlx v1.3.4
	github.com/labstack/echo/v4 v4.7.2
	github.com/labstack/gommon v0.3.1
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.2.0
	github.com/mitchellh/mapstructure v1.4.3
	github.com/spf13/viper v1.10.1
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
