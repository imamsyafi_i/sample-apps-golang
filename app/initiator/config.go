package initiator

import (
	"log"
	"os"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/sample-apps/internal/model"
	"github.com/spf13/viper"
)

var (
	// ConfigType name
	configType = "json"

	// Global Config
	globalConfig model.Config
)

// NewConfig load app config
func NewConfig() model.Config {
	return globalConfig
}

// InitConfig initialize config to viper
func InitConfig() {
	// Set Credentials to Viper Reader
	env := os.Getenv("ENVIRONMENT")
	creds := os.Getenv("CREDENTIALS")

	if env == "" {
		log.Fatalf("ENVIRONMENT not found")
	}

	if creds != "" {
		// Read from Env
		viper.SetConfigType(configType)
		err := viper.ReadConfig(strings.NewReader(creds))
		if err != nil {
			log.Fatalf("unable to read in config file into viper, %v", err)
		}
	} else {
		// Read from File
		viper.SetConfigFile("config/config.json")
		viper.ReadInConfig()
	}

	// Mapping Credentials from Viper Reader
	var credentials map[string]interface{}
	conf := &model.Config{}

	// Map credentials
	err := viper.Unmarshal(&credentials)
	if err != nil {
		log.Fatalf("unable to decode into config struct, %v", err)
	}

	// Map config by env
	err = mapstructure.Decode(credentials[env], &conf)
	if err != nil {
		log.Fatalf("unable to decode into config struct, %v", err)
	}

	globalConfig = *conf
	initTimeZone()
}

func initTimeZone() {
	// Set time to local Asia/Jakarta
	loc, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		log.Fatalf("unable to load location time, %v", err)
	}
	time.Local = loc // -> this is setting the global timezone
}
