package initiator

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/color"
	"github.com/sample-apps/internal/controller"
	"github.com/sample-apps/internal/database"
	"github.com/sample-apps/internal/model"
	"github.com/sample-apps/internal/repository"
	"github.com/sample-apps/internal/usecase"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func Run() {
	//call func InitConfig
	InitConfig()

	//init echo
	r := echo.New()
	r.Validator = &CustomValidator{validator: validator.New()}

	// Config
	log.Println("[INFO] Loading config")
	cfg := NewConfig()

	// DB
	log.Println("[INFO] Loading database")
	postgresDB := database.NewPostgresDatabase(cfg)

	// Repository
	log.Println("[INFO] Loading repository")
	userRepo := repository.NewUserRepository(postgresDB)

	// Usecase
	log.Println("[INFO] Loading usecase")
	userUsecase := usecase.NewUserUsecase(userRepo)

	// Controller
	log.Println("[INFO] Loading controller")
	userController := controller.NewUserController(userUsecase)

	// Routes
	log.Println("[INFO] Loading routes")
	NewRoutes(cfg, r, userController)

	// Server Runner
	log.Println("[INFO] Loading server")
	serverRunner(cfg, r)

}

func serverRunner(
	cfg model.Config,
	r *echo.Echo) {

	go func() {
		if err := r.Start(fmt.Sprintf("%s:%s", cfg.Host.Address, cfg.Host.Port)); err != nil && err != http.ErrServerClosed {
			r.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 30 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	sig := <-quit
	fmt.Println()
	color.Println(color.Green(fmt.Sprint("received shutdown signal. Trying to shutdown gracefully ", sig)))
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := r.Shutdown(ctx); err != nil {
		log.Fatal("Failure while shutting down gracefully, errApp: ", err)
	}
	color.Println(color.Green("Shutdown gracefully completed"))
}
