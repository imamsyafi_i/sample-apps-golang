package initiator

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sample-apps/internal/controller"
	"github.com/sample-apps/internal/model"
)

func NewRoutes(
	config model.Config,
	r *echo.Echo,
	userController controller.UserController,
) http.Handler {
	//Middleware
	setMiddlewareGlobal(r)

	r.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "hello, its sample apps")
	})

	r.GET("/user", func(c echo.Context) error {
		return c.String(http.StatusOK, userController.CobaController())
	})

	r.GET("/show-user", userController.ShowUser)
	r.GET("/show-user-byid", userController.ShowUserById)
	r.GET("/show-user-byid/:id", userController.ShowUserById)

	r.POST("/show-user-limit", userController.ShowUserByLimit)

	r.POST("/create-user", userController.CreateUser)
	r.POST("/bulk-create-user", userController.BulkCreateUser)

	r.PUT("/update-user", userController.UpdateUser)

	r.DELETE("/delete-user", userController.DeleteUser)

	r.POST("/create-update-user", userController.CreateUpdateUser)

	return r
}

// setMiddlewareGlobal set middleware global
func setMiddlewareGlobal(r *echo.Echo) {
	// Logger
	r.Use(middleware.Logger())

	r.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"https://*", "http://*"},
		AllowHeaders:     []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization, echo.HeaderXCSRFToken},
		AllowMethods:     []string{http.MethodGet, http.MethodPost, http.MethodDelete, http.MethodPut},
		AllowCredentials: false,
		ExposeHeaders:    []string{"Link"},
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	// Recovery
	r.Use(middleware.Recover())
}
