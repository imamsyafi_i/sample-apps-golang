package utility

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/sample-apps/internal/database"
)

const (
	ContextKeySqlxTx = "sqlx_tx"
)

type DbTx interface {
	sqlx.ExtContext
	Rollback() error
	Commit() error
}

// SetSqlxTxInContext set db connection in context
func SetSqlxTxInContext(ctx context.Context, db DbTx) context.Context {
	return context.WithValue(ctx, ContextKeySqlxTx, db)
}

//CheckTxMaster check tx or not on db master
func CheckTxMaster(ctx context.Context, db database.PostgresDatabase) sqlx.ExtContext {
	sqlxTx := ctx.Value(ContextKeySqlxTx)
	if sqlxTx == nil {
		return db.PostgresMaster
	}

	return sqlxTx.(sqlx.ExtContext)
}

//CheckTxSlave check tx or not on db slave
func CheckTxSlave(ctx context.Context, db database.PostgresDatabase) sqlx.ExtContext {
	sqlxTx := ctx.Value(ContextKeySqlxTx)
	if sqlxTx == nil {
		return db.PostgresSlave
	}

	return sqlxTx.(sqlx.ExtContext)
}
