package usecase

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/labstack/gommon/log"
	"github.com/sample-apps/internal/model"
	"github.com/sample-apps/internal/repository"
	"github.com/sample-apps/utility"
)

//interface
type UserUsecase interface {
	ShowUser() (res model.UserResponse)
	ShowUserById(ctx context.Context, id int64) (res *model.UserResponse, err error)
	ShowUserWithLimit(ctx context.Context, limit int64) (res []model.UserResponse, err error)
	CreateUser(ctx context.Context, req model.UserCreateRequest) (lastInsertId *int64, err error)
	CreateBulkUser(ctx context.Context, req model.BulkUserCreateRequest) (err error)
	UpdateUser(ctx context.Context, req model.UserUpdateRequest) (affectedRow int64, err error)
	DeleteUser(ctx context.Context, req model.UserDeleteRequest) (err error)
	CreateUpdateUser(ctx context.Context, req model.UserCreateRequest) (err error)
}

// implement interface
type UserUsecaseImpl struct {
	userRepo repository.UserRepository
}

// NewReportRepository init report repo
func NewUserUsecase(userRepo repository.UserRepository) UserUsecase {
	return UserUsecaseImpl{
		userRepo: userRepo,
	}
}

// ShowUser show user static data
func (uc UserUsecaseImpl) ShowUser() (res model.UserResponse) {
	res.Id = 1
	res.Nama = "Coba User"
	return res
}

// ShowUserById show user by id from database
func (uc UserUsecaseImpl) ShowUserById(ctx context.Context, id int64) (res *model.UserResponse, err error) {
	resultData, err := uc.userRepo.GetDataByID(ctx, id)
	if err != nil {
		log.Errorf("error on GetDataByID. error: %v", err)
		return res, err
	}
	if resultData == nil {
		return res, err
	}

	resMap := mapDataUserResponse(*resultData)
	return &resMap, err
}

// mapDataUserResponse map UserCobaModel to UserResponse
func mapDataUserResponse(req model.UserCobaModel) (result model.UserResponse) {
	result.Id = req.Id
	result.Nama = req.Nama.String
	return result
}

// ShowUserWithLimit show user with limit from database
func (uc UserUsecaseImpl) ShowUserWithLimit(ctx context.Context, limit int64) (res []model.UserResponse, err error) {
	resultData, err := uc.userRepo.GetDataUserByLimit(ctx, limit)
	if err != nil {
		log.Errorf("error on GetDataByID. error: %v", err)
		return res, err
	}

	resMap := mapListDataUserResponse(resultData)
	return resMap, err
}

// mapDataUserResponse map UserCobaModel to UserResponse
func mapListDataUserResponse(req []model.UserCobaModel) (result []model.UserResponse) {
	for _, v := range req {
		row := model.UserResponse{
			Id:   v.Id,
			Nama: v.Nama.String,
		}
		result = append(result, row)
	}
	return result
}

func (uc UserUsecaseImpl) CreateUser(ctx context.Context, req model.UserCreateRequest) (lastInsertId *int64, err error) {
	reqModel := model.UserCobaModel{
		Nama: sql.NullString{String: req.Nama, Valid: true},
	}
	lastInsertId, err = uc.userRepo.InsertDataUser(ctx, reqModel)
	if err != nil {
		return nil, err
	}
	return lastInsertId, err
}

func (uc UserUsecaseImpl) CreateBulkUser(ctx context.Context, req model.BulkUserCreateRequest) (err error) {
	arrReqModel := make([]model.UserCobaModel, 0)
	for _, v := range req.User {
		reqModel := model.UserCobaModel{
			Nama: sql.NullString{String: v.Nama, Valid: true},
		}
		arrReqModel = append(arrReqModel, reqModel)
	}
	err = uc.userRepo.BulkInsertDataUser(ctx, arrReqModel)
	return err
}

func (uc UserUsecaseImpl) UpdateUser(ctx context.Context, req model.UserUpdateRequest) (affectedRow int64, err error) {
	reqModel := model.UserCobaModel{
		Nama: sql.NullString{String: req.Nama, Valid: true},
	}
	affectedRow, err = uc.userRepo.UpdateDataUser(ctx, reqModel, req.Id)
	if err != nil {
		return affectedRow, err
	}
	return affectedRow, err
}

func (uc UserUsecaseImpl) DeleteUser(ctx context.Context, req model.UserDeleteRequest) (err error) {
	err = uc.userRepo.DeleteDataUser(ctx, req.Id)
	if err != nil {
		return err
	}
	return nil
}

func (uc UserUsecaseImpl) CreateUpdateUser(ctx context.Context, req model.UserCreateRequest) (err error) {
	trx, err := uc.userRepo.Tx(ctx)
	if err != nil {
		return err
	}
	defer trx.Rollback()

	ctxTx := utility.SetSqlxTxInContext(ctx, trx)

	lastInsertId, err := uc.CreateUser(ctxTx, req)
	if err != nil {
		return err
	}

	if lastInsertId != nil {
		reqUpdate := model.UserUpdateRequest{
			Id:   *lastInsertId,
			Nama: fmt.Sprint(req.Nama, "-", *lastInsertId),
		}
		_, err := uc.UpdateUser(ctxTx, reqUpdate)
		if err != nil {
			return err
		}
	}

	// Commit transactions
	if err := trx.Commit(); err != nil {
		return err
	}

	return err
}
