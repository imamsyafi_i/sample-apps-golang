package repository

import (
	"context"
	"database/sql"

	"github.com/labstack/gommon/log"
	"github.com/sample-apps/internal/database"
	"github.com/sample-apps/internal/model"
	"github.com/sample-apps/utility"
)

//interface
type UserRepository interface {
	GetDataByID(ctx context.Context, id int64) (result *model.UserCobaModel, err error)
	GetDataUserByLimit(ctx context.Context, limit int64) (result []model.UserCobaModel, err error)
	InsertDataUser(ctx context.Context, req model.UserCobaModel) (LastInsertId *int64, err error)
	BulkInsertDataUser(ctx context.Context, req []model.UserCobaModel) (err error)
	UpdateDataUser(ctx context.Context, req model.UserCobaModel, id int64) (affectedRow int64, err error)
	DeleteDataUser(ctx context.Context, id int64) (err error)
	Tx(ctx context.Context) (utility.DbTx, error)
}

// implement interface
type UserRepositoryImpl struct {
	postgresDB database.PostgresDatabase
}

// UserRepository init user repo
func NewUserRepository(postgresDB database.PostgresDatabase) UserRepository {
	return UserRepositoryImpl{
		postgresDB: postgresDB,
	}
}

func (r UserRepositoryImpl) GetDataByID(ctx context.Context, id int64) (result *model.UserCobaModel, err error) {
	// init database
	db := r.postgresDB.PostgresSlave
	userData := model.UserCobaModel{}

	query := `select id, nama from user_coba where id = $1 `

	// Exec query
	err = db.QueryRowxContext(ctx, query, id).StructScan(&userData)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Errorf("r.db.QueryRowxContext GetProvinceByIdProvince err: %v", err)
		return &userData, err
	}

	return &userData, err
}

func (r UserRepositoryImpl) GetDataUserByLimit(ctx context.Context, limit int64) (result []model.UserCobaModel, err error) {
	// init database
	db := r.postgresDB.PostgresSlave

	query := `select id, nama from user_coba limit $1 `

	// Query Scan
	rows, err := db.QueryxContext(ctx, query, limit)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Errorf("r.db.QueryxContext GetDataUserByLimit err: %v", err)
		return result, err
	}

	// Iterate to struct
	for rows.Next() {
		userData := model.UserCobaModel{}
		err := rows.StructScan(&userData)
		if err != nil {
			log.Errorf("StructScan GetDataUserByLimit err: %v", err)
			return result, err
		}
		result = append(result, userData)
	}

	defer func() {
		if err = rows.Close(); err != nil {
			log.Errorf("rows.Close() err: %v", err)
			return
		}
	}()

	return result, err
}

func (r UserRepositoryImpl) InsertDataUser(ctx context.Context, req model.UserCobaModel) (LastInsertId *int64, err error) {
	// init database
	db := utility.CheckTxMaster(ctx, r.postgresDB)

	query := `insert into user_coba (nama) values($1) RETURNING id`

	// Query Scan
	err = db.QueryRowxContext(ctx, query, req.Nama.String).Scan(&LastInsertId)
	if err != nil {
		log.Errorf("r.db.Exec InsertDataUser err: %v", err)
		return LastInsertId, err
	}

	return LastInsertId, err
}

func (r UserRepositoryImpl) BulkInsertDataUser(ctx context.Context, req []model.UserCobaModel) (err error) {
	// init database
	db := r.postgresDB.PostgresMaster

	query := `insert into user_coba(nama) values(:nama)`

	// Query Scan
	_, err = db.NamedExecContext(ctx, query, req)
	if err != nil {
		log.Errorf("r.db.Exec InsertDataUser err: %v", err)
		return err
	}

	return err
}

func (r UserRepositoryImpl) UpdateDataUser(ctx context.Context, req model.UserCobaModel, id int64) (affectedRow int64, err error) {
	// init database
	db := utility.CheckTxMaster(ctx, r.postgresDB)

	query := `update user_coba set nama = $1 where id = $2`

	// Query Scan
	sqlResult, err := db.ExecContext(ctx, query, req.Nama.String, id)
	if err != nil {
		log.Errorf("r.db.Exec UpdateDataUser err: %v", err)
		return affectedRow, err
	}

	affectedRow, err = sqlResult.RowsAffected()
	if err != nil {
		log.Errorf("affectedRow err: %v", err)
		return affectedRow, err
	}
	return affectedRow, err
}

func (r UserRepositoryImpl) DeleteDataUser(ctx context.Context, id int64) (err error) {
	// init database
	db := r.postgresDB.PostgresMaster

	query := `delete from user_coba where id = $1`

	// Query Scan
	_, err = db.ExecContext(ctx, query, id)
	if err != nil {
		log.Errorf("r.db.Exec UpdateDataUser err: %v", err)
		return err
	}

	return err
}

func (r UserRepositoryImpl) Tx(ctx context.Context) (utility.DbTx, error) {
	// init database
	db := r.postgresDB.PostgresMaster

	return db.BeginTxx(ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
}
