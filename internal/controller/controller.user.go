package controller

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sample-apps/internal/model"
	"github.com/sample-apps/internal/usecase"
)

//interface
type UserController interface {
	CobaController() string
	ShowUser(ec echo.Context) error
	ShowUserById(ec echo.Context) error
	ShowUserByLimit(ec echo.Context) error
	CreateUser(ec echo.Context) error
	BulkCreateUser(ec echo.Context) error
	UpdateUser(ec echo.Context) error
	DeleteUser(ec echo.Context) error
	CreateUpdateUser(ec echo.Context) error
}

// implement interface
type UserControllerImpl struct {
	UserUC usecase.UserUsecase
}

// NewReportController init report controller
func NewUserController(UserUC usecase.UserUsecase) UserController {
	return &UserControllerImpl{
		UserUC: UserUC,
	}
}

func (r UserControllerImpl) CobaController() string {
	return "output from coba controller"
}

func (r UserControllerImpl) ShowUser(ec echo.Context) error {
	res := r.UserUC.ShowUser()
	ec.JSON(http.StatusOK, res)
	return nil
}

func (r UserControllerImpl) ShowUserById(ec echo.Context) error {
	ctx := ec.Request().Context()

	// unmarshal raw body into struct
	requestParam := model.UserRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "param id must a number")
		return err
	}

	// validate request data
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "param limit required")
		return err
	}

	id := requestParam.Id
	res, err := r.UserUC.ShowUserById(ctx, id)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err)
		return nil
	}

	if res == nil {
		ec.JSON(http.StatusOK, "Data Not Found")
		return nil
	}

	ec.JSON(http.StatusOK, res)
	return nil
}

func (r UserControllerImpl) ShowUserByLimit(ec echo.Context) error {
	ctx := ec.Request().Context()

	// unmarshal raw body into struct
	requestParam := model.UserListRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "param limit must a number")
		return err
	}

	// validate request data
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "param limit required")
		return err
	}

	limit := requestParam.Limit
	res, err := r.UserUC.ShowUserWithLimit(ctx, limit)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err)
		return nil
	}

	ec.JSON(http.StatusOK, res)
	return nil
}

func (r UserControllerImpl) CreateUser(ec echo.Context) error {
	ctx := ec.Request().Context()

	// unmarshal raw body into struct
	requestParam := model.UserCreateRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	// validate request data
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	res, err := r.UserUC.CreateUser(ctx, requestParam)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err)
		return nil
	}

	if res != nil {
		msg := "insert success"
		ec.JSON(http.StatusOK, msg)
	}
	return nil
}

func (r UserControllerImpl) BulkCreateUser(ec echo.Context) error {
	ctx := ec.Request().Context()

	// unmarshal raw body into struct
	requestParam := model.BulkUserCreateRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	// validate request data
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	err := r.UserUC.CreateBulkUser(ctx, requestParam)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err)
		return nil
	}

	msg := "insert success"
	ec.JSON(http.StatusOK, msg)
	return nil
}

func (r UserControllerImpl) UpdateUser(ec echo.Context) error {
	ctx := ec.Request().Context()

	// unmarshal raw body into struct
	requestParam := model.UserUpdateRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	// validate request data
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	_, err := r.UserUC.UpdateUser(ctx, requestParam)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err)
		return nil
	}

	msg := "update success"
	ec.JSON(http.StatusOK, msg)

	return nil
}

func (r UserControllerImpl) DeleteUser(ec echo.Context) error {
	ctx := ec.Request().Context()

	// unmarshal raw body into struct
	requestParam := model.UserDeleteRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	// validate request data
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	err := r.UserUC.DeleteUser(ctx, requestParam)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err.Error())
		return nil
	}

	msg := "deleted success"
	ec.JSON(http.StatusOK, msg)

	return nil
}

func (r UserControllerImpl) CreateUpdateUser(ec echo.Context) error {
	ctx := ec.Request().Context()

	// unmarshal raw body into struct
	requestParam := model.UserCreateRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	// validate request data
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "check your parameter")
		return err
	}

	err := r.UserUC.CreateUpdateUser(ctx, requestParam)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err.Error())
		return nil
	}

	msg := "insert + update success"
	ec.JSON(http.StatusOK, msg)

	return nil
}
