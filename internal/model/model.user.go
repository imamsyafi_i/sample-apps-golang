package model

import "database/sql"

type UserResponse struct {
	Id   int64  `json:"id"`
	Nama string `json:"nama"`
}

type UserCobaModel struct {
	Id   int64          `json:"id"`
	Nama sql.NullString `json:"nama"`
}

type UserRequest struct {
	Id int64 `param:"id" query:"id" json:"id" validate:"required,number"`
}

type UserListRequest struct {
	Limit int64 `form:"limit" json:"limit" validate:"required,number"`
}

type UserCreateRequest struct {
	Nama string `form:"nama" json:"nama" validate:"required"`
}

type BulkUserCreateRequest struct {
	User []UserCreateRequest `json:"user"`
}

type UserUpdateRequest struct {
	Id   int64  `form:"id" json:"id" validate:"required,number"`
	Nama string `form:"nama" json:"nama" validate:"required"`
}

type UserDeleteRequest struct {
	Id int64 `form:"id" json:"id" validate:"required,number"`
}
